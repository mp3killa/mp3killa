package com.example.mp3killa;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Calendar;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity {

    ScheduledExecutorService execT = Executors.newScheduledThreadPool(1);
    ScheduledExecutorService execP = Executors.newScheduledThreadPool(1);
    TextView tvDegrees;
    TextView tvPrime;
    static int NumberToCheck = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tvDegrees = findViewById(R.id.tvDegrees);
        tvPrime = findViewById(R.id.tvPrime);

        execT.scheduleAtFixedRate(calcDegrees, 0, 1, TimeUnit.MINUTES);
        execP.scheduleAtFixedRate(calcPrime, 0, 2, TimeUnit.SECONDS);

    }

    Runnable calcDegrees = new Runnable() {
        public void run() {
            try {
                Calendar cal = Calendar.getInstance();
                int hours = cal.get(Calendar.HOUR_OF_DAY);
                int minutes = cal.get(Calendar.MINUTE);

                double angle_hours = (hours * (360 / 12));
                double angle_minutes = (minutes * (360 / 60));
                double angle_hours_travel = .5 * minutes;
                angle_hours = angle_hours_travel + angle_hours;
                final double angles_between = Math.abs(angle_hours - angle_minutes);
                runOnUiThread(new Runnable() {
                    public void run() {
                        tvDegrees.setText(String.valueOf(angles_between));
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    Runnable calcPrime = new Runnable() {
        public void run() {
            try {
                NumberToCheck++;
                final boolean isPrime = isPrimeNumber(NumberToCheck);
                runOnUiThread(new Runnable() {
                    public void run() {
                        tvPrime.setText(String.format(NumberToCheck + " prime " + isPrime));
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    public static boolean isPrimeNumber(int numberToCheck) {
        int factors = 0;
        int remainder = 0;
        int divideBy = 1;

        while (divideBy <= numberToCheck) {
            if (numberToCheck % divideBy == remainder) {
                factors++;
            }
            divideBy++;
        }
        return (factors == 2);
    }
}
